"""Tests for Closed-Form matting and foreground/background solver."""
import unittest

import cv2
import numpy as np
import os
import time
from numba import njit
import closed_form_matting

@njit
def opt_merge(ori_im, alpha, fg):
    w, h, c = alpha.shape
    for i in range(w):
        for j in range(h):
            fg[i, j][0] = int(ori_im[i, j][0] * float(alpha[i, j][0]) / 255 + 255 * (1 - alpha[i, j][0] / 255.0))  # %255      #+ (1-float(mask[i,j])/255)*bg_img[i,j]
            fg[i, j][1] = int(ori_im[i, j][1] * float(alpha[i, j][1]) / 255 + 0 * (1 - alpha[i, j][1] / 255.0))  # %255
            fg[i, j][2] = int(ori_im[i, j][2] * float(alpha[i, j][2]) / 255 + 0 * (1 - alpha[i, j][2] / 255.0))  # %255
    return fg

@njit
def opt_merge_bg(ori_im, alpha, fg,bg_im):
    w, h, c = alpha.shape
    for i in range(w):
            for j in range(h):

                fg[i,j][0] = int(ori_im[i,j][0]*float(alpha[i,j][0])/255 + (1-float(alpha[i,j][0])/255)*bg_im[i,j][0])
                fg[i, j][1] = int(ori_im[i, j][1] * float(alpha[i, j][1]) / 255 + (1 - float(alpha[i, j][1]) / 255) * \
                                  bg_im[i, j][1])
                fg[i, j][2] = int(ori_im[i, j][2] * float(alpha[i, j][2]) / 255 + (1 - float(alpha[i, j][2]) / 255) * \
                                  bg_im[i, j][2])
    return fg

def merge(alpha_img,ori_img,bg_img=None):
    '''
        图像融合
    :param alpha_img:   标注的像素级mask图
    :param ori_img:     原图
    :param bg_img       需要融合的背景图
    :return:
    '''
    alpha = cv2.imread(alpha_img)
    ori_im = cv2.imread(ori_img)
    fg = np.zeros(ori_im.shape)
    print(alpha.shape)
    if bg_img is None:
        fg = opt_merge(ori_im, alpha, fg)
    else:
        bg_im = cv2.imread(bg_img)
        fg = opt_merge_bg(ori_im, alpha, fg, bg_im)
    return fg

if __name__=="__main__":
    trimap_dir = "./trimap/"
    image_dir = "./image/"
    alpha_dir = "./alpha/"
    merged_dir = "./merged/"
    if not os.path.exists(alpha_dir):
        os.mkdir(alpha_dir)
    trimaps = os.listdir(trimap_dir)
    for trimap in trimaps:
        tmp_trimap = os.path.join(trimap_dir,trimap)
        tmp_ori_image = os.path.join(image_dir,trimap)
        tmp_alpha = os.path.join(alpha_dir,trimap)
        tmp_merge = os.path.join(merged_dir,trimap)
        print("generate original image %s, trimap %s into alpha %s"%(tmp_ori_image, tmp_trimap, tmp_alpha))
        begin = time.time()
        image_arr = cv2.imread(tmp_ori_image, cv2.IMREAD_COLOR) / 255.0
        trimap_arr = cv2.imread(tmp_trimap, cv2.IMREAD_GRAYSCALE) / 255.0
        alpha = closed_form_matting.closed_form_matting_with_trimap(image_arr, trimap_arr)
        cv2.imwrite(tmp_alpha,alpha*255.0)
        meg = merge(tmp_alpha, tmp_ori_image)
        cv2.imwrite(tmp_merge, meg)
        end = time.time()
        print("image merge time cost:\t",(end-begin))
