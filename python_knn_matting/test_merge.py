import cv2
import numpy as np
import os
from numba import njit
import scipy
from knn_matting import knn_matte
import time
@njit
def opt_merge(ori_im, alpha, fg):
    w, h, c = alpha.shape
    for i in range(w):
        for j in range(h):
            fg[i, j][0] = int(ori_im[i, j][0] * float(alpha[i, j][0]) / 255 + 255 * (
                        1 - alpha[i, j][0] / 255.0))  # %255      #+ (1-float(trimap[i,j])/255)*bg_img[i,j]
            fg[i, j][1] = int(ori_im[i, j][1] * float(alpha[i, j][1]) / 255 + 0 * (1 - alpha[i, j][1] / 255.0))  # %255
            fg[i, j][1] = int(ori_im[i, j][2] * float(alpha[i, j][2]) / 255 + 0 * (1 - alpha[i, j][2] / 255.0))  # %255
    return fg

@njit
def opt_merge_bg(ori_im, alpha, fg,bg_im):
    w, h, c = alpha.shape
    for i in range(w):
            for j in range(h):

                fg[i,j][0] = int(ori_im[i,j][0]*float(alpha[i,j][0])/255 + (1-float(alpha[i,j][0])/255)*bg_im[i,j][0])
                fg[i, j][1] = int(ori_im[i, j][1] * float(alpha[i, j][1]) / 255 + (1 - float(alpha[i, j][1]) / 255) * \
                                  bg_im[i, j][1])
                fg[i, j][2] = int(ori_im[i, j][2] * float(alpha[i, j][2]) / 255 + (1 - float(alpha[i, j][2]) / 255) * \
                                  bg_im[i, j][2])

def merge(alpha_img,ori_img,bg_img=None):
    '''
        图像融合
    :param alpha_img:   标注的像素级mask图
    :param ori_img:     原图
    :param bg_img       需要融合的背景图
    :return:
    '''
    alpha = cv2.imread(alpha_img)
    ori_im = cv2.imread(ori_img)
    fg = np.zeros(ori_im.shape)
    print(alpha.shape)
    if bg_img is None:
        fg = opt_merge(ori_im, alpha, fg)
    else:
        bg_im = cv2.imread(bg_img)
        fg = opt_merge_bg(ori_im, alpha, fg, bg_im)
    return fg

def trimap_alpha2merge(trimap_path,ori_img_path,alpha_path,merge_path):
    '''
         根据原图和trimap图生成精确的alpha图，以及根据原图和alpha图合成最终融合的背景图
    :param trimap_path:      需要提供的 粗略地trimap
    :param ori_img_path:     原始RGB图路径
    :param alpha_path:        算法计算得到的精确alpha图最终存放路径
    :param merge_path:        使用alpha图从原图中扣除人物并与蓝色背景融合的图存放路径
    :return:
    '''
    begin = time.time()
    img = scipy.misc.imread(ori_img_path)[:, :, :3]
    trimap = scipy.misc.imread('donkeyTrimap.png')[:,:,:3]
    print("bayesian matting from trimap %s and original image %s begin.."%(trimap_path,ori_img_path))
    alpha = knn_matte(img, trimap)
    scipy.misc.imsave(alpha_path, alpha)
    print("merge alpha image %s and original image %s into result %s ."%(alpha_path,ori_img_path,merge_path))
    meg = merge(alpha_path, ori_img_path)
    cv2.imwrite(merge_path, meg)
    end = time.time()
    print("one image %s merged ,time cost:\t"%trimap_path,(end-begin))

alpha_dir = "./alpha/"
ori_img_dir = "./image/"
trimap_dir = "./trimap/"
merged_dir= "./merged/"
ori_img_dir = "/home/xiatao/data/imgs/faces/DeepAutomaticPortraitMatting/deeplab/ori_img/"
#ori_img_path = "/home/xiatao/work/bayesian-matting/image/"

for test_img in os.listdir(trimap_dir):
    trimap_path = trimap_dir+test_img
    ori_img_path = ori_img_dir+test_img
    alpha_path =  alpha_dir+ test_img
    merge_path = merged_dir + test_img
    trimap_alpha2merge(trimap_path, ori_img_path, alpha_path, merge_path)

