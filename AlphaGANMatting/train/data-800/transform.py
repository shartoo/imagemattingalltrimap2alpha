from PIL import Image as image
from skimage import data,filters,img_as_ubyte,feature
from skimage import io,transform,color,restoration,morphology
from skimage.filters import rank as sfr
import matplotlib.pyplot as plt
import numpy as np
import os
import cv2



srcdir = './bg'
dstdir = './dst'

files = os.listdir(srcdir)

for file in files:
    absolutepath = os.path.join(srcdir,file)
    srcimage = io.imread(absolutepath)
    srcimage = transform.resize(srcimage,(800,800)) * 255
    srcimage = srcimage.astype(np.uint8)
    dstsolutepath = os.path.join(dstdir,file)
    io.imsave(dstsolutepath,srcimage)
	
