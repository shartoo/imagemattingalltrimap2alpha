import cv2
import numpy as np
import os

def extract_fg(alpha_img,ori_img):
    '''
        从原图中抽取出前景图
    :param alpha_img:   标注的像素级mask图
    :param ori_img:     原图
    :return:
    '''
    alpha = cv2.imread(alpha_img)
    ori_im = cv2.imread(ori_img)

    fg = np.zeros(ori_im.shape)
    print(alpha.shape)
    w,h,c = alpha.shape
    for i in range(w):
        for j in range(h):
            fg[i,j][0] = int(ori_im[i,j][0]*float(alpha[i,j][0])/255)
            fg[i, j][1] = int(ori_im[i, j][1] * float(alpha[i,j][1])/255)
            fg[i, j][2] = int(ori_im[i, j][2] * float(alpha[i,j][2])/255)
    return fg


alpha_path = "./train/alpha"
input_path = "./train/input"
imgs = [os.path.join(alpha_path,x) for x in os.listdir(alpha_path)]
for img in imgs:
    input = img.replace("alpha","input")
    fg = extract_fg(img,input)
    new_name = os.path.join("./train/fg/",os.path.basename(img))
    print("new name is :\t",new_name)
    cv2.imwrite(new_name,fg)
    print("finished one img ",img)

