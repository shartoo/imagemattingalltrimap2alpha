from skimage import data,color,morphology,feature
import cv2
import scipy.ndimage as ndi
from skimage import morphology
import matplotlib.pyplot as plt
import os
import numpy as np

trimap_dir = "/home/xiatao/work/global-matting/trimap_ori/"
trimap_save = "/home/xiatao/work/global-matting/trimap/"
for trimap in os.listdir(trimap_dir):
    src = cv2.imread(os.path.join(trimap_dir,trimap))
    # Denoising
    dst = cv2.fastNlMeansDenoisingColored(src,None,10,10,7,21)
    ## b.设置卷积核5*5
    #kernel = np.ones((5, 5), np.uint8)
    #dst = cv2.dilate(src,kernel)
    ## c.图像的腐蚀，默认迭代次数
    #erosion = cv2.erode(dst, kernel)
    ## 腐蚀后
    cv2.imwrite(os.path.join(trimap_save,trimap), dst)
    print("denoising image %s finished.."%trimap)

