#include "globalmatting.h" 
#include <typeinfo>
#include <string> 
#include <iostream> 
#include <experimental/filesystem>
using namespace std;
namespace fs = std::experimental::filesystem; 
// you can get the guided filter implementation from https://github.com/atilimcetin/guided-filter
#include "guidedfilter.h"

int main() {
    std::string image_path= "./image/";
    std::string trimap_path = "./trimap/";
    std::string alpha_path = "./alpha/";
    std::string alpha_before_path = "./alpha_before/";

    for (const auto & entry : fs::directory_iterator(trimap_path))
   {
    std::cout << entry.path()<< std::endl;
    
    std::string tmp_img = image_path+entry.path().filename().string();
   
    std::string tmp_trimap = trimap_path + entry.path().filename().string();
    std::string tmp_alpha = alpha_path+ entry.path().filename().string();
    std::string tmp_before_alpha = alpha_before_path+ entry.path().filename().string();

    cv::Mat image = cv::imread(tmp_img, CV_LOAD_IMAGE_COLOR);
    cv::Mat trimap = cv::imread(tmp_trimap, CV_LOAD_IMAGE_GRAYSCALE);
    

    // (optional) exploit the affinity of neighboring pixels to reduce the
    // size of the unknown region. please refer to the paper
    // 'Shared Sampling for Real-Time Alpha Matting'.
  
    
    expansionOfKnownRegions(image, trimap, 9);
    cout<<"processing image "<<tmp_img<<endl;
    cv::Mat foreground, alpha;
    globalMatting(image, trimap, foreground, alpha);
    

    // filter the result with fast guided filter
   
    cv::Mat alpha1 = guidedFilter(image, alpha, 10, 1e-5);
    for (int x = 0; x < trimap.cols; ++x)
        for (int y = 0; y < trimap.rows; ++y)
        {
            if (trimap.at<uchar>(y, x) == 0)
                alpha1.at<uchar>(y, x) = 0;
            else if (trimap.at<uchar>(y, x) ==255)
                alpha1.at<uchar>(y, x) = 255;
        }
   for (int x=0;x<alpha1.cols;++x)
   {
    for(int y=0;y<alpha1.rows;++y)
    {
     if(alpha1.at<uchar>(y,x)<128)
      alpha1.at<uchar>(y,x) = 0;
    }
   }
    cv::imwrite(tmp_before_alpha, alpha);
    cv::imwrite(tmp_alpha,alpha1);
   
   }
    return 0;
}
