#include "globalmatting.h" 
#include <typeinfo>
#include <string> 
#include <iostream> 
#include <experimental/filesystem>
using namespace std;
namespace fs = std::experimental::filesystem; 
// you can get the guided filter implementation from https://github.com/atilimcetin/guided-filter
#include "guidedfilter.h"

int main(int argc,char* argv[]) {

    if(argc < 4)
    {
            std::cout<<"缺少参数"; 
            return 0;
    }
    std::string image_path = argv[1];
    std::string trimap_path = argv[2];
    std::string alpha_path = argv[3];
    std::cout<<image_path<<trimap_path<<alpha_path<<std::endl;
    //return 0;
    //std::string alpha_before_path = "./alpha_before/";

    
    //std::string tmp_img = image_path+entry.path().filename().string();
   
    //std::string tmp_trimap = trimap_path + entry.path().filename().string();
    //std::string tmp_alpha = alpha_path+ entry.path().filename().string();
    //std::string tmp_before_alpha = alpha_before_path+ entry.path().filename().string();

    cv::Mat image = cv::imread(image_path, CV_LOAD_IMAGE_COLOR);
    cv::Mat trimap = cv::imread(trimap_path, CV_LOAD_IMAGE_GRAYSCALE);
    

    // (optional) exploit the affinity of neighboring pixels to reduce the
    // size of the unknown region. please refer to the paper
    // 'Shared Sampling for Real-Time Alpha Matting'.
  
    
    expansionOfKnownRegions(image, trimap, 9);
    //cout<<"processing image "<<tmp_img<<endl;
    cv::Mat foreground, alpha;
    globalMatting(image, trimap, foreground, alpha);
    

    // filter the result with fast guided filter
   
    cv::Mat alpha1 = guidedFilter(image, alpha, 10, 1e-5);
    for (int x = 0; x < trimap.cols; ++x)
        for (int y = 0; y < trimap.rows; ++y)
        {
            if (trimap.at<uchar>(y, x) == 0)
                alpha1.at<uchar>(y, x) = 0;
            else if (trimap.at<uchar>(y, x) ==255)
                alpha1.at<uchar>(y, x) = 255;
        }
   for (int x=0;x<alpha1.cols;++x)
   {
    for(int y=0;y<alpha1.rows;++y)
    {
     if(alpha1.at<uchar>(y,x)<128)
      alpha1.at<uchar>(y,x) = 0;
    }
   }
    //cv::imwrite(tmp_before_alpha, alpha);
    cv::imwrite(alpha_path,alpha1);
   
    return 0;
}
