#!/bin/bash
trimap_dir="./trimap/"
ori_img_dir="./image/"
alpha_dir="./alpha/"
trimaps=`ls ${trimap_dir}`
for tri_mp in ${trimaps}
do
	ori_img=$ori_img_dir$tri_mp
    #echo $ori_img
	real_trimap=$trimap_dir$tri_mp
	save_alpha=$alpha_dir$tri_mp
	starttime=`date +'%Y-%m-%d %H:%M:%S'`	
	./build/RobustMatting ${ori_img} ${real_trimap} ${save_alpha}
	endtime=`date +'%Y-%m-%d %H:%M:%S'`
	start_seconds=$(date --date="$starttime" +%s);
	end_seconds=$(date --date="$endtime" +%s);
	echo "image matting image $real_trimap finished!"
	echo "time cost  "$((end_seconds-start_seconds))"s"
done
